#!/bin/sh
# this script cleans the source code of uhexen2 to make it dfsg
# it should run from topdir of the source code

# delete svn entries
find . -type d -name .svn |xargs rm -rf

find . -type f -regextype posix-extended -iregex '.*/VisualStudio20[0-9]+.zip' | xargs rm -f
find . -type f -regextype posix-extended -iregex '.*/build_cross_win(32|64).sh' | xargs rm -f
find . -type f -regextype posix-extended -iregex '.*/build_cross_(osx|dos).sh' | xargs rm -f
find . -type f -regextype posix-extended -iregex '.*/build_(osx|dos|w64|w32).sh' | xargs rm -f

rm -rf gamecode/patch111
rm -rf gamecode/hc/siege
rm -rf gamecode/hc-unused
rm -rf gamecode/txt/siege
rm -rf LICENSE.txt hexen2.spec
rm -rf engine/hexen2/dos/
rm -rf engine/hexen2/MacOSX
rm -rf docs/README.dos
rm -rf docs/README.win32
rm -rf docs/README.AoT
rm -rf docs/COMPILE
rm -rf docs/COPYING
rm -rf scripts/ccase.sh
rm -rf scripts/cross_defs.dj
rm -rf scripts/cross_defs.w32
rm -rf scripts/cross_defs.w64
rm -rf scripts/gcc-version.sh
rm -rf scripts/mk_header.c
rm -rf scripts/mkrelease.sh
rm -rf scripts/systest.c
rm -rf patches/syntax_checking.patch
