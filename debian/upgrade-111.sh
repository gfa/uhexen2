#!/bin/sh
#
# this script will download the binaries to patch pak0.pak and pak1.pak files 
# from uhexen2 site and patch them.
# you need to be root or at least be able to write to /usr/share/games/hexen2
# gustavo panizzo <gfa@zumbi.com.ar> 2012-09-17

GDVER="1.28"
URL="http://downloads.sourceforge.net/project/uhexen2/Hexen2%20GameData/gamedata-${GDVER}/gamedata-src-${GDVER}.tgz"

TEMP=`mktemp -d`

for tool in wget xdelta3 tar md5sum awk
do
    which $tool >/dev/null 2>/dev/null
if [ $? != 0 ]; then
        echo "$tool is not avaiable, it is needed for this script to work"
        exit 1
    fi
done

PAK0_MD5=`md5sum /usr/share/games/hexen2/data1/pak0.pak |awk '{print $1}'`
PAK1_MD5=`md5sum /usr/share/games/hexen2/data1/pak1.pak |awk '{print $1}'`

wget ${URL} -O ${TEMP}/gamedata-${GDVER}.tgz

tar xzf ${TEMP}/gamedata-${GDVER}.tgz -C ${TEMP}/

if [ $PAK0_MD5 = c9675191e75dd25a3b9ed81ee7e05eff ]; then
    echo  "pak0.pak file is already updated to 1.11"
    else
    mv /usr/share/games/hexen2/data1/pak0.pak $TEMP/
    xdelta3 -f -d -s ${TEMP}/pak0.pak ${TEMP}/gamecode-${GDVER}/patch111/patchdat/data1/data1pk0.xd3 /usr/share/games/hexen2/data1/pak0.pak
    #xdelta3 -f -d -s /usr/share/games/hexen2/data1/pak0.pak ${TEMP}/gamecode-${GDVER}/patch111/patchdat/data1/data1pk0.xd3 /usr/share/games/hexen2/data1/pak0.pak
fi

if [ $PAK1_MD5 = c2ac5b0640773eed9ebe1cda2eca2ad0 ]; then
    echo  "pak1.pak file is already updated to 1.11"
    else
    mv /usr/share/games/hexen2/data1/pak1.pak $TEMP/
    xdelta3 -f -d -s ${TEMP}/pak1.pak ${TEMP}/gamecode-${GDVER}/patch111/patchdat/data1/data1pk1.xd3 /usr/share/games/hexen2/data1/pak1.pak
fi

rm -rf ${TEMP}
